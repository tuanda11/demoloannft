// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract LoanNft is Ownable {
    // variables and mappings
    IERC721 public tokenContract;
    IERC20 public paymentToken;
    
    // structs and events 

    // MAIN FUNCTION
    constructor(address _nftAddress, address _paymentToken ) {
        tokenContract = IERC721(_nftAddress);
        paymentToken = IERC20(_paymentToken);
    }

    function changeNFTAddress(address _nftAddress) external onlyOwner {
        tokenContract = IERC721(_nftAddress);
    }

    function changeTokenAddress(address _paymentToken) external onlyOwner {
        paymentToken = IERC20(_paymentToken);
    }

    /**
      @param _nftId: The id of the nft
      @param _initialOwner: the owner who lend nft for others
     */
    function hireNFT(uint256 _nftId, address _initialOwner, uint256 _value) public {
        paymentToken.transferFrom(msg.sender, _initialOwner, _value);
        tokenContract.transferFrom(_initialOwner, msg.sender, _nftId);
    }

    /**
      @param _nftId: The id of the nft
      @param _initialOwner: the owner who lend nft for others
     */
    function returnNFT(uint256 _nftId, address _initialOwner) public {
        tokenContract.transferFrom(msg.sender, _initialOwner, _nftId);
    }
}
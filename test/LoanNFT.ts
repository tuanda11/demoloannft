import { time, loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Loan NFT", () => {
  async function deployContractsAndGetSigners() {
    const [deployer, alice, bob, daniel] = await ethers.getSigners();

    const NFT = await ethers.getContractFactory("NFT721");
    const nft = await (await NFT.deploy()).deployed();

    const LoanNFT = await ethers.getContractFactory("LoanNft");
    const loanNft = await (await LoanNFT.deploy(nft.address)).deployed();

    return { nft, loanNft, deployer, alice, bob, daniel };
  }

  it("Should lend nft successfully", async () => {
    const { nft, loanNft, deployer, alice, bob, daniel } = await loadFixture(
      deployContractsAndGetSigners
    );

    // mint nft for alice
    await nft.mint(alice.address);
    const aliceBalance = await nft.balanceOf(alice.address);
    expect(aliceBalance).to.equal("1");

    // approve for bob the nft id 1
    await nft.connect(alice).approve(loanNft.address, "1");
    // hire NFT
    await loanNft.connect(bob).hireNFT("1", alice.address);

    const bobBalance = await nft.balanceOf(bob.address);
    expect(bobBalance).to.equal("1");
  });

  it("Should return nft successfully", async () => {
    const { nft, loanNft, deployer, alice, bob, daniel } = await loadFixture(
      deployContractsAndGetSigners
    );

    // mint nft for alice
    await nft.mint(alice.address);

    // approve for bob the nft id 1
    await nft.connect(alice).approve(loanNft.address, "1");
    // hire NFT
    await loanNft.connect(bob).hireNFT("1", alice.address);

    await nft.connect(bob).approve(loanNft.address, "1");
    await loanNft.connect(bob).returnNFT("1", alice.address);

    const aliceBalance = await nft.balanceOf(alice.address);

    expect(aliceBalance).to.equal("1");
  });
});
